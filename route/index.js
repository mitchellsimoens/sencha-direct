
module.exports = {
    get Express () {
        return require('./Express');
    },

    get Mixin () {
        return require('./Mixin');
    }
};
