module.exports = {
    get Action () {
        return require('./Action');
    },

    get Directable () {
        return require('./Directable');
    },

    get Manager () {
        return require('./Manager');
    },

    get Provider () {
        return require('./Provider');
    },

    get route () {
        return require('./route');
    }
};
